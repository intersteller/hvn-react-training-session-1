import React, { Component } from "react";

const Homepage = () => {
  const click = () => {
    alert("OK");
  };

  return <h1 onClick={() => click()}>Homepage</h1>
};

export default Homepage;
